package hr.demo.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.NameViewHolder> {

    private List<String> dataList;
    private OnClickListener clickListener;

    public CustomAdapter(List<String> dataList, OnClickListener clickListener) {
        this.dataList = dataList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public NameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        return new NameViewHolder(listItem, clickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NameViewHolder holder, int position) {
        Log.d("CustomAdapter", "Name on position " + position + " is " + dataList.get(position));
        holder.setName(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public static class NameViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;
        private final ImageView deleteImg;

        public NameViewHolder(@NonNull View itemView, OnClickListener clickListener) {
            super(itemView);

            textView = itemView.findViewById(R.id.textView);
            deleteImg = itemView.findViewById(R.id.iv_delete);

            deleteImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onClick(getAdapterPosition());
                }
            });
        }

        public void setName(String name) {
            textView.setText(name);
        }
    }

    public void addItem(String name, int position) {
        dataList.add(name);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        dataList.remove(position);
        notifyItemRemoved(position);
    }
}
