package hr.demo.RecyclerView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnClickListener {

    private RecyclerView reciclerView;
    private List<String> dataList;
    private Button btn_add_name;
    private EditText et_add_name;
    private CustomAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupDataList();
        setupRecyclerView();
        setupView();
    }

    private void setupRecyclerView() {
        reciclerView = findViewById(R.id.recycledView);

        reciclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new CustomAdapter(dataList, this);
        reciclerView.setAdapter(adapter);
    }

    private void setupDataList() {
        dataList = new ArrayList<>();

        dataList.add("Item1");
        dataList.add("Item2");
        dataList.add("Item3");
        dataList.add("Item4");
    }

    private void setupView() {
        this.btn_add_name = (Button) findViewById(R.id.btn_add_name);
        this.et_add_name = (EditText) findViewById(R.id.et_add_name);

        this.btn_add_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.addItem(et_add_name.getText().toString(), adapter.getItemCount());
                et_add_name.setText("");
            }
        });
    }

    @Override
    public void onClick(int position) {
        adapter.removeItem(position);
    }
}