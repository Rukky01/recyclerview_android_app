
# RecyclerView_Android_App
#### This android app is created as a faculty project and as such has no real use-case just to show learned skill.
It features a list of data that can be edited by adding new items or deleting current items.

# Functionality
* On click on X, that item will be deleted from the list.
* On click on ADD text from EditText will be added to the list.
### Developed
This project is developed by [Luka Ruskan.](https://lukaruskan.com/)
### Preview
![Preview](preview.png)
